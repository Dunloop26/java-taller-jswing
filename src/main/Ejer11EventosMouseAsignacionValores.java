package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.SwingConstants;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.Box;

public class Ejer11EventosMouseAsignacionValores extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer11EventosMouseAsignacionValores frame = new Ejer11EventosMouseAsignacionValores();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer11EventosMouseAsignacionValores() {
		setMaximumSize(new Dimension(440, 300));
		setMinimumSize(new Dimension(200, 240));
		setTitle("Eventos del mouse y asignación de valores");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 440, 299);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlBody = new JPanel();
		contentPane.add(pnlBody, BorderLayout.CENTER);
		pnlBody.setLayout(new GridLayout(0, 3, 0, 0));
		
		JLabel lblNueve = new JLabel("9");
		lblNueve.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblNueve);
		
		JLabel lblOcho = new JLabel("8");
		lblOcho.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblOcho);
		
		JLabel lblSiete = new JLabel("7");
		lblSiete.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblSiete);
		
		JLabel lblSeis = new JLabel("6");
		lblSeis.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblSeis);
		
		JLabel lblCinco = new JLabel("5");
		lblCinco.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblCinco);
		
		JLabel lblCuatro = new JLabel("4");
		lblCuatro.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblCuatro);
		
		JLabel lblTres = new JLabel("3");
		lblTres.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblTres);
		
		JLabel lblDos = new JLabel("2");
		lblDos.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblDos);
		
		JLabel lblUno = new JLabel("1");
		lblUno.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblUno);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		pnlBody.add(horizontalGlue);
		
		JLabel lblCero = new JLabel("0");
		lblCero.setHorizontalAlignment(SwingConstants.CENTER);
		pnlBody.add(lblCero);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		pnlBody.add(horizontalGlue_1);
		
		JPanel pnlHeader = new JPanel();
		contentPane.add(pnlHeader, BorderLayout.NORTH);
		
		JLabel lblNumeros = new JLabel("Numeros");
		lblNumeros.setHorizontalAlignment(SwingConstants.CENTER);
		pnlHeader.add(lblNumeros);
		
		JPanel pnlFooter = new JPanel();
		contentPane.add(pnlFooter, BorderLayout.SOUTH);
		GridBagLayout gbl_pnlFooter = new GridBagLayout();
		gbl_pnlFooter.columnWidths = new int[]{110, 75, 0};
		gbl_pnlFooter.rowHeights = new int[]{25, 0};
		gbl_pnlFooter.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_pnlFooter.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		pnlFooter.setLayout(gbl_pnlFooter);
		
		txtNumero = new JTextField();
		GridBagConstraints gbc_txtNumero = new GridBagConstraints();
		gbc_txtNumero.fill = GridBagConstraints.BOTH;
		gbc_txtNumero.insets = new Insets(0, 0, 0, 5);
		gbc_txtNumero.gridx = 0;
		gbc_txtNumero.gridy = 0;
		pnlFooter.add(txtNumero, gbc_txtNumero);
		txtNumero.setColumns(20);
		
		JButton btnVaciar = new JButton("Vaciar");
		btnVaciar.setEnabled(false);
		GridBagConstraints gbc_btnVaciar = new GridBagConstraints();
		gbc_btnVaciar.fill = GridBagConstraints.BOTH;
		gbc_btnVaciar.gridx = 1;
		gbc_btnVaciar.gridy = 0;
		pnlFooter.add(btnVaciar, gbc_btnVaciar);
		btnVaciar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtNumero.setText("");
				btnVaciar.setEnabled(false);
			}
		});
		txtNumero.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				updateBtnState(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
			}
			
			private void updateBtnState(DocumentEvent e) {
				java.awt.EventQueue.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						btnVaciar.setEnabled(true);
					}
				});
			}
		});
		
		MouseListener lblMouseListener = new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				if(e.getSource().getClass() == JLabel.class) {
					JLabel label = (JLabel)e.getSource();
					label.setOpaque(false);
					label.setForeground(Color.black);
				}
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				if(e.getSource().getClass() == JLabel.class) {
					JLabel label = (JLabel)e.getSource();
					txtNumero.setText(txtNumero.getText() + label.getText());
					label.setOpaque(true);
					label.setBackground(Color.black);
					label.setForeground(Color.gray);
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		};
		JLabel[] labels = {
			lblCero,
			lblUno,
			lblDos,
			lblTres,
			lblCuatro,
			lblCinco,
			lblSeis,
			lblSiete,
			lblOcho,
			lblNueve
		};
		
		for(JLabel label : labels) {
			label.addMouseListener(lblMouseListener);
		}
	}

}
