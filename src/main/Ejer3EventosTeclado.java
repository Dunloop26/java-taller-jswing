package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

public class Ejer3EventosTeclado extends JFrame {

	private JPanel contentPane;
	private JLabel lblTexto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer3EventosTeclado frame = new Ejer3EventosTeclado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer3EventosTeclado() {
		setTitle("Eventos del teclado");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextField txtTexto = new JTextField();
		txtTexto.setBounds(27, 48, 390, 40);
		contentPane.add(txtTexto);
		txtTexto.setColumns(10);
		txtTexto.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				int[] modifierKeys = {
						16, 17, 18, 8, 127, 27, 65406
				};
				
				for(int key : modifierKeys) {
					if (key == e.getKeyCode()) return;
				}
				
				char key = e.getKeyChar();
				lblTexto.setText(lblTexto.getText() + key);
			}
		});
		
		lblTexto = new JLabel("Etiqueta Texto");
		lblTexto.setBounds(37, 99, 380, 15);
		contentPane.add(lblTexto);
		
		JButton btnVaciar = new JButton("Vaciar");
		btnVaciar.setBounds(167, 130, 117, 25);
		contentPane.add(btnVaciar);
		btnVaciar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtTexto.setText("");
				lblTexto.setText("");
			}
		});
	}
}
