package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JButton;

public class Ejer2PasarInformacion extends JFrame {

	private JPanel contentPane;
	private JLabel lblEtiqueta1;
	private JLabel lblEtiqueta2;
	private JTextField txtTexto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer2PasarInformacion frame = new Ejer2PasarInformacion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer2PasarInformacion() {
		setTitle("Pasar información entre componentes gráficos");
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtTexto = new JTextField();
		txtTexto.setBounds(22, 40, 405, 34);
		contentPane.add(txtTexto);
		txtTexto.setColumns(10);
		
		lblEtiqueta1 = new JLabel("Etiqueta 1");
		lblEtiqueta1.setHorizontalAlignment(SwingConstants.CENTER);
		lblEtiqueta1.setBounds(32, 86, 185, 20);
		contentPane.add(lblEtiqueta1);
		
		lblEtiqueta2 = new JLabel("Etiqueta 2");
		lblEtiqueta2.setHorizontalAlignment(SwingConstants.CENTER);
		lblEtiqueta2.setBounds(229, 91, 198, 15);
		contentPane.add(lblEtiqueta2);
		
		JButton btnTraspasa1 = new JButton("Traspasa 1");
		btnTraspasa1.setBounds(66, 118, 117, 25);
		contentPane.add(btnTraspasa1);
		btnTraspasa1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = txtTexto.getText();
				lblEtiqueta1.setText(text);
			}
		});
		
		JButton btnTraspasa2 = new JButton("Traspasa 2");
		btnTraspasa2.setBounds(269, 118, 117, 25);
		contentPane.add(btnTraspasa2);
		btnTraspasa2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String text = txtTexto.getText();
				lblEtiqueta2.setText(text);
			}
		});
	}
}
