package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;

public class Ejer8ManejoColoresEtiquetas extends JFrame {

	private JPanel contentPane;
	private JLabel lblPrograma;
	private JButton btnRojo;
	private JButton btnAzul;
	private JButton btnVerde;
	private JButton btnFondoAzul;
	private JButton btnFondoRojo;
	private JButton btnTransparente;
	private JButton btnOpaca;
	private JButton btnFondoVerde;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer8ManejoColoresEtiquetas frame = new Ejer8ManejoColoresEtiquetas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer8ManejoColoresEtiquetas() {
		setTitle("Manejo de colores y propiedades de las etiquetas");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblPrograma = new JLabel("ProgramaJava");
		lblPrograma.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrograma.setBounds(128, 12, 174, 54);
		contentPane.add(lblPrograma);

		ActionListener btnActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == btnRojo) {
					lblPrograma.setForeground(new Color(200, 20, 20));
				} else if (e.getSource() == btnAzul) {
					lblPrograma.setForeground(new Color(20, 20, 200));
				} else if (e.getSource() == btnVerde) {
					lblPrograma.setForeground(new Color(20, 200, 20));
				} else if (e.getSource() == btnFondoAzul) {
					lblPrograma.setBackground(new Color(20, 20, 200));
				} else if (e.getSource() == btnFondoRojo) {
					lblPrograma.setBackground(new Color(200, 20, 20));
				} else if (e.getSource() == btnFondoVerde) {
					lblPrograma.setBackground(new Color(20, 200, 20));
				} else if (e.getSource() == btnTransparente || e.getSource() == btnOpaca) {
					lblPrograma.setOpaque(e.getSource() == btnOpaca);
				}
				
				repaint();
			}
		};

		btnRojo = new JButton("Rojo");
		btnRojo.setBounds(88, 78, 117, 25);
		contentPane.add(btnRojo);

		btnAzul = new JButton("Azul");
		btnAzul.setBounds(88, 115, 117, 25);
		contentPane.add(btnAzul);

		btnVerde = new JButton("Verde");
		btnVerde.setBounds(88, 152, 117, 25);
		contentPane.add(btnVerde);

		btnFondoAzul = new JButton("Fondo Azul");
		btnFondoAzul.setBounds(238, 115, 133, 25);
		contentPane.add(btnFondoAzul);

		btnFondoRojo = new JButton("Fondo Rojo");
		btnFondoRojo.setBounds(238, 78, 133, 25);
		contentPane.add(btnFondoRojo);

		btnFondoVerde = new JButton("Fondo Verde");
		btnFondoVerde.setBounds(238, 152, 133, 25);
		contentPane.add(btnFondoVerde);

		btnTransparente = new JButton("Transparente\n");
		btnTransparente.setBounds(238, 189, 133, 25);
		contentPane.add(btnTransparente);

		btnOpaca = new JButton("Opaca");
		btnOpaca.setBounds(88, 189, 117, 25);
		contentPane.add(btnOpaca);
		
		JButton[] buttons = {
				btnAzul,
				btnRojo,
				btnVerde,
				btnFondoRojo, 
				btnFondoAzul,
				btnFondoVerde,
				btnTransparente,
				btnOpaca
		};
		
		for(JButton button : buttons) {
			button.addActionListener(btnActionListener);
		}
	}

}
