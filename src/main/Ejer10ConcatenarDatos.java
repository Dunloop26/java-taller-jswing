package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Ejer10ConcatenarDatos extends JFrame {

	private JPanel contentPane;
	private JButton btnConcatena;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer10ConcatenarDatos frame = new Ejer10ConcatenarDatos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer10ConcatenarDatos() {
		setResizable(false);
		setTitle("Concatenar datos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextField txtPalabra = new JTextField();
		txtPalabra.setText("El lobo es");
		txtPalabra.setBounds(44, 71, 177, 19);
		contentPane.add(txtPalabra);
		txtPalabra.setColumns(10);
		
		JTextField txtPalabra_1 = new JTextField();
		txtPalabra_1.setText(" carnivoro por naturaleza");
		txtPalabra_1.setBounds(233, 71, 184, 19);
		contentPane.add(txtPalabra_1);
		txtPalabra_1.setColumns(10);
		
		JLabel lblTexto = new JLabel("");
		lblTexto.setHorizontalAlignment(SwingConstants.CENTER);
		lblTexto.setBounds(57, 137, 334, 15);
		contentPane.add(lblTexto);
		
		btnConcatena = new JButton("Concatena");
		btnConcatena.setBounds(166, 173, 117, 25);
		contentPane.add(btnConcatena);
		
		JLabel lblPalabras = new JLabel("Palabras");
		lblPalabras.setBounds(189, 32, 70, 15);
		contentPane.add(lblPalabras);
		
		JLabel lblResultadoConcatenado = new JLabel("Resultado Concatenado");
		lblResultadoConcatenado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultadoConcatenado.setBounds(62, 110, 324, 15);
		contentPane.add(lblResultadoConcatenado);
		btnConcatena.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String palabra1 = txtPalabra.getText();
				String palabra2 = txtPalabra_1.getText();
				
				lblTexto.setText(palabra1 + palabra2);
			}
		});
	}
}
