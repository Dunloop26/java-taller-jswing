package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Ejer4DesactivarComponentes extends JFrame {

	private JPanel contentPane;
	private JLabel lblFrase;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer4DesactivarComponentes frame = new Ejer4DesactivarComponentes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer4DesactivarComponentes() {
		setTitle("Desactivar componentes gráficos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextField txtCiudad = new JTextField();
		txtCiudad.setText("Ciudad");
		txtCiudad.setBounds(230, 67, 158, 19);
		contentPane.add(txtCiudad);
		txtCiudad.setColumns(10);
		
		JTextField txtNombre = new JTextField();
		txtNombre.setText("Nombre");
		txtNombre.setBounds(56, 67, 152, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		lblFrase = new JLabel("Frase");
		lblFrase.setHorizontalAlignment(SwingConstants.CENTER);
		lblFrase.setBounds(56, 115, 332, 15);
		contentPane.add(lblFrase);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(160, 189, 117, 25);
		contentPane.add(btnAceptar);
		btnAceptar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String name = txtNombre.getText();
				String ciudad = txtCiudad.getText();
				lblFrase.setText(String.format("Usted se llama %s y vive en %s", name, ciudad));
			}
		});
		
		JButton btnActivar = new JButton("Activar");
		btnActivar.setBounds(91, 153, 117, 25);
		contentPane.add(btnActivar);
		btnActivar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtCiudad.setEnabled(true);
				txtNombre.setEnabled(true);
			}
		});
		
		JButton btnDesactivar = new JButton("Desactivar");
		btnDesactivar.setBounds(230, 153, 117, 25);
		contentPane.add(btnDesactivar);
		btnDesactivar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				txtCiudad.setEnabled(false);
				txtNombre.setEnabled(false);
			}
		});
	}
}
