package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.SwingConstants;

public class Ejer6EventosMousePosicionamiento extends JFrame {

	private JPanel contentPane;
	private JLabel lblNombre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer6EventosMousePosicionamiento frame = new Ejer6EventosMousePosicionamiento();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer6EventosMousePosicionamiento() {
		setResizable(false);
		setTitle("Eventos del mouse y posicionamiento");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("John James Valencia");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBackground(Color.RED);
		lblNombre.setOpaque(true);
		lblNombre.setBounds(124, 44, 201, 40);
		contentPane.add(lblNombre);
		
		JButton btnEsquina = new JButton("Esquina");
		btnEsquina.setBounds(234, 103, 117, 25);
		contentPane.add(btnEsquina);
		btnEsquina.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblNombre.setLocation(0,0);
			}
		});
		
		JButton btnCentro = new JButton("Centro");
		btnCentro.setBounds(234, 142, 117, 25);
		contentPane.add(btnCentro);
		btnCentro.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblNombre.setLocation(149, 66);
			}
		});
		
		JButton btnAchicar = new JButton("Achicar");
		btnAchicar.setBounds(89, 142, 117, 25);
		contentPane.add(btnAchicar);
		btnAchicar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Rectangle r = lblNombre.getBounds();
				r.x += 5;
				r.y += 5;
				r.width -= 10;
				r.height -= 10;
				lblNombre.setBounds(r);
			}
		});
		
		JButton btnAgrandar = new JButton("Agrandar");
		btnAgrandar.setBounds(89, 103, 117, 25);
		contentPane.add(btnAgrandar);
		btnAgrandar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Rectangle r = lblNombre.getBounds();
				r.x -= 5;
				r.y -= 5;
				r.width += 10;
				r.height += 10;
				lblNombre.setBounds(r);
			}
		});
	}

}
