package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;

public class Ejer9NotaEstudiantes extends JFrame {

	private JPanel contentPane;
	private JFormattedTextField frmtdtxtfldPrimertrimestre;
	private JFormattedTextField frmtdtxtfldSegundotrimestre;
	private JFormattedTextField frmtdtxtfldTercertrimestre;
	private JLabel lblNumeroNotaFinal;
	private JLabel lblPalabraResultadoFinal;
	private JLabel lblNotasEstudiante;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer9NotaEstudiantes frame = new Ejer9NotaEstudiantes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer9NotaEstudiantes() {
		setTitle("Nota de estudiantes");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		NumberFormat floatFormat = NumberFormat.getNumberInstance();
		
		
		frmtdtxtfldPrimertrimestre = new JFormattedTextField(floatFormat);
		frmtdtxtfldPrimertrimestre.setColumns(10);
		frmtdtxtfldPrimertrimestre.setBounds(45, 58, 114, 19);
		contentPane.add(frmtdtxtfldPrimertrimestre);
		
		frmtdtxtfldSegundotrimestre = new JFormattedTextField(floatFormat);
		frmtdtxtfldSegundotrimestre.setColumns(10);
		frmtdtxtfldSegundotrimestre.setBounds(168, 58, 114, 19);
		contentPane.add(frmtdtxtfldSegundotrimestre);
		
		frmtdtxtfldTercertrimestre = new JFormattedTextField(floatFormat);
		frmtdtxtfldTercertrimestre.setColumns(10);
		frmtdtxtfldTercertrimestre.setBounds(289, 58, 114, 19);
		contentPane.add(frmtdtxtfldTercertrimestre);
		
		JLabel lblNotafinal = new JLabel("Nota Final:");
		lblNotafinal.setBounds(45, 111, 90, 15);
		contentPane.add(lblNotafinal);
		
		JLabel lblResultado = new JLabel("Resultado:");
		lblResultado.setBounds(45, 146, 90, 15);
		contentPane.add(lblResultado);
		
		lblNumeroNotaFinal = new JLabel("0");
		lblNumeroNotaFinal.setBounds(168, 111, 235, 15);
		contentPane.add(lblNumeroNotaFinal);
		
		lblPalabraResultadoFinal = new JLabel("0");
		lblPalabraResultadoFinal.setVerticalAlignment(SwingConstants.TOP);
		lblPalabraResultadoFinal.setBounds(168, 146, 235, 15);
		contentPane.add(lblPalabraResultadoFinal);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.setBounds(163, 201, 117, 25);
		contentPane.add(btnCalcular);
		
		lblNotasEstudiante = new JLabel("Notas Estudiante");
		lblNotasEstudiante.setHorizontalAlignment(SwingConstants.CENTER);
		lblNotasEstudiante.setBounds(41, 12, 362, 15);
		contentPane.add(lblNotasEstudiante);
		
		JFrame frame = this;
		btnCalcular.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				double nota1, nota2, nota3;
				try {					
					nota1 = Double.parseDouble(frmtdtxtfldPrimertrimestre.getText());
					nota2 = Double.parseDouble(frmtdtxtfldSegundotrimestre.getText());
					nota3 = Double.parseDouble(frmtdtxtfldTercertrimestre.getText());
				} catch (NumberFormatException ex) {
					ex.printStackTrace();
					JOptionPane.showMessageDialog(frame, "Números incorrectos. por favor ingrese una cantidad adecuada.", "Alerta", JOptionPane.ERROR_MESSAGE);
					lblNumeroNotaFinal.setText("0");
					return;
				}
				
				double promedio = (nota1 + nota2 + nota3) / 3;
				
				if (promedio >= 5) {
					lblPalabraResultadoFinal.setText("APROBADO");
					lblPalabraResultadoFinal.setForeground(Color.black);
				} else {
					lblPalabraResultadoFinal.setText("SUSPENSO");
					lblPalabraResultadoFinal.setForeground(Color.red);
				}
				
				lblNumeroNotaFinal.setText(String.valueOf((double) Math.round(promedio * 100) / 100));
			}
		});
	}
}
