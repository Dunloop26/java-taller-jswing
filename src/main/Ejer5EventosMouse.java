package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Color;

public class Ejer5EventosMouse extends JFrame {

	private JPanel contentPane;
	private JLabel lblPalabra1;
	private JLabel lblPalabra2;
	private JLabel lblPalabra3;
	private JLabel lblPalabra4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer5EventosMouse frame = new Ejer5EventosMouse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer5EventosMouse() {
		setResizable(false);
		setTitle("Eventos del mouse");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblPalabra1 = new JLabel("Gato");
		lblPalabra1.setHorizontalAlignment(SwingConstants.CENTER);
		lblPalabra1.setBounds(24, 101, 70, 15);
		contentPane.add(lblPalabra1);
		
		lblPalabra2 = new JLabel("Perro");
		lblPalabra2.setHorizontalAlignment(SwingConstants.CENTER);
		lblPalabra2.setBounds(128, 101, 70, 15);
		contentPane.add(lblPalabra2);
		
		lblPalabra3 = new JLabel("Hamster");
		lblPalabra3.setHorizontalAlignment(SwingConstants.CENTER);
		lblPalabra3.setBounds(232, 101, 70, 15);
		contentPane.add(lblPalabra3);
		
		lblPalabra4 = new JLabel("Serpiente");
		lblPalabra4.setHorizontalAlignment(SwingConstants.CENTER);
		lblPalabra4.setBounds(336, 101, 70, 15);
		contentPane.add(lblPalabra4);
		
		JLabel lblOcultar = new JLabel("Ocultar");
		lblOcultar.setBackground(Color.GRAY);
		lblOcultar.setOpaque(true);
		lblOcultar.setForeground(Color.WHITE);
		lblOcultar.setFont(new Font("Dialog", Font.BOLD, 16));
		lblOcultar.setHorizontalAlignment(SwingConstants.CENTER);
		lblOcultar.setBounds(166, 148, 88, 33);
		contentPane.add(lblOcultar);
		lblOcultar.addMouseListener(new MouseListener() {
			
			private JLabel[] labels = {
					lblPalabra1,
					lblPalabra2,
					lblPalabra3,
					lblPalabra4
			};
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				for(JLabel label: labels) {
					label.setVisible(true);
				}				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				for(JLabel label: labels) {
					label.setVisible(false);
				}
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}

}
