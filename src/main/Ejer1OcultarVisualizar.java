package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingConstants;

public class Ejer1OcultarVisualizar extends JFrame {

	private JPanel contentPane;
	private JLabel lblNombre;
	private JLabel lblCiudad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejer1OcultarVisualizar frame = new Ejer1OcultarVisualizar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ejer1OcultarVisualizar() {
		setTitle("Ocultar y Visualizar Componentes Gráficos");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblNombre = new JLabel("John James Valencia");
		lblNombre.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombre.setBounds(33, 53, 175, 15);
		contentPane.add(lblNombre);
		
		lblCiudad = new JLabel("La Tebaida");
		lblCiudad.setHorizontalAlignment(SwingConstants.CENTER);
		lblCiudad.setBounds(242, 53, 175, 15);
		contentPane.add(lblCiudad);
		
		JButton btnOcultarNombre = new JButton("Ocultar Nombre");
		btnOcultarNombre.setBounds(33, 166, 175, 25);
		contentPane.add(btnOcultarNombre);
		btnOcultarNombre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {	
				lblNombre.setVisible(false);
			}
		});
		
		JButton btnMostrarNombre = new JButton("Mostrar Nombre");
		btnMostrarNombre.setBounds(33, 129, 175, 25);
		contentPane.add(btnMostrarNombre);
		btnMostrarNombre.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblNombre.setVisible(true);
			}
		});
		
		JButton btnOcultarCiudad = new JButton("Ocultar Ciudad");
		btnOcultarCiudad.setBounds(242, 166, 175, 25);
		contentPane.add(btnOcultarCiudad);
		btnOcultarCiudad.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblCiudad.setVisible(false);
			}
		});
		
		JButton btnMostrarCiudad = new JButton("Mostrar Ciudad");
		btnMostrarCiudad.setBounds(242, 129, 175, 25);
		contentPane.add(btnMostrarCiudad);
		btnMostrarCiudad.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				lblCiudad.setVisible(true);
			}
		});
	}
}
